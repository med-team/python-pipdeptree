Source: python-pipdeptree
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-setuptools
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pipdeptree
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pipdeptree.git
Homepage: https://github.com/naiquevin/pipdeptree
Rules-Requires-Root: no

Package: python3-pipdeptree
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: python3-graphviz
Description: display dependency tree of the installed Python 3 packages
 Pipdeptree is a command line utility for displaying the installed Python
 packages in form of a dependency tree. It works for packages installed
 globally on a machine as well as in a virtualenv. Since pip freeze shows
 all dependencies as a flat list, finding out which are the top level
 packages and which packages do they depend on requires some effort. It
 can also be tedious to resolve conflicting dependencies because pip
 doesn't yet have true dependency resolution (more on this later). This
 utility tries to solve this problem.
 .
 To some extent, this tool is inspired by lein deps :tree command of
 Leiningen.
